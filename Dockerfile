FROM python:3.8-slim

LABEL maintainer="tylerfrench2@gmail.com"

# Setup user
RUN useradd pde
RUN groupadd pdevents
RUN usermod -a -G pdevents pde

WORKDIR /opt/pdevents

# Set file permissions
RUN chgrp pdevents /opt/pdevents && chgrp -R pdevents /opt/pdevents
RUN chown pde /opt/pdevents && chown -R pde /opt/pdevents

# Copy Static files and requirements
COPY requirements.txt setup.cfg setup.py LICENSE.md README.md /opt/pdevents/
RUN pip install -r requirements.txt

# Copy latest object changes
ADD PagerDutyEvents /opt/pdevents/PagerDutyEvents

# Install Package
RUN python setup.py install

# Set runtime user
USER pde