from itertools import islice

import pypd
import threading


class PagerDutyEventsCache(object):
    def __init__(self, api_key: str = None, integration_key: str = None):
        self.cache = {
            "cache_key": api_key + integration_key,
            "data": []
        }

    def add_event(self, event: dict = None):
        self.cache['data'].extend(event)

    def get_cache(self) -> list:
        return self.cache['data']

    def __str__(self):
        return f"Key: {self.cache['cache_key']}\nData: {self.cache['data']}"

    def __repr__(self):
        return f"Key: {self.cache['cache_key']}\nData: {self.cache['data']}"

    def __getitem__(self, item):
        if item == "data":
            return self.cache['data']
        else:
            return self.cache['cache_key']

    def __len__(self):
        return len(self.cache['data'])


class PagerDutyEvents(object):
    def __init__(self, client: pypd = None, api_key: str = None, integration_key: str = None,
                 cache: PagerDutyEventsCache = None):
        if client is None:
            self.client = self._ignition(api_key)
        else:
            self.client = client
        if integration_key is None:
            raise Exception("No integration key passed")
        else:
            self.integration_key = integration_key
        if api_key is None:
            raise Exception("No api key passed")
        else:
            self.api_key = api_key

        if cache is not None:
            if isinstance(cache, PagerDutyEventsCache):
                self.cache = cache
            else:
                raise Exception("Incorrect cache type: {}").__format__(type(cache))
        else:
            self.cache = PagerDutyEventsCache(api_key=self.api_key, integration_key=self.integration_key)

        self.event_template = {
            'routing_key': self.integration_key,
            'event_action': 'trigger',
            'client': 'Default',
            'payload': {
                'severity': 'info',
                'custom_details': {
                    'alert_id': None
                }
            }
        }

    @staticmethod
    def _ignition(api_key: str = None) -> pypd:
        client = pypd
        client.api_key = api_key
        return client

    @staticmethod
    def _split_data(data: PagerDutyEventsCache = None, limit=100):
        it = iter(data.cache['data'])
        for i in range(0, len(data), limit):
            yield {k: data[k] for k in islice(it, limit)}

    def _create_batch(self, data: dict = None):
        if self.client.api_key == "TESTAPIKEY":
            for event in data:
                print(event)
        else:
            for event in data:
                self.client.Event.create(event)

    def create_limited(self):
        # Create local threads list
        threads = []

        # Create threads and thread data
        for thread_data in self._split_data(self.cache):
            t_data = threading.local()
            t_data.data = dict(thread_data)
            thread = threading.Thread(target=self._create_batch, args=(t_data.data,))
            threads.append(thread)

        # Start threads
        for thread in threads:
            thread.start()

        # Join threads together
        for thread in threads:
            thread.join()

    def load_events(self, event_data: list = None):
        # Load all the events into cache
        for event in event_data:
            self.cache.add_event(event)

