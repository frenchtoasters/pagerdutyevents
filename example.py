from PagerDutyEvents import pagerdutyevents

pdes = pagerdutyevents.PagerDutyEvents(api_key="testapikey", integration_key="testintegrationkey")

events = {0: {"fo": "bar"}, 1: {"foo": "barr"}, 2: {"fooo": "barrr"}, 3: {"foooo": "barrrr"}}

pdes.load_events(events)

pdes.create_limited()