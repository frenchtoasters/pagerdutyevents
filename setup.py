from setuptools import setup, find_packages
import PagerDutyEvents

setup(
    name="PagerDutyEvents",
    version=PagerDutyEvents.__version__,
    author=PagerDutyEvents.__author__,
    description="Create up to 100 pagerduty events asynchronously",
    long_description=open('README.md').read(),
    url='https://gitlab.com/frenchtoasters/pagerdutyevents',
    keywords=['tools', 'events', 'pagerduty', 'async', 'ratelimit', 'interface'],
    license=PagerDutyEvents.__license__,
    packages=find_packages(exclude=['*.test', '*.test.*']),
    include_package_date=True,
    install_requires=open('requirements.txt').readlines(),
    entry_point={
        'console_scripts': [
            'pdes=PagerDutyEvents.pagdutyevents:PagerDutyEvents'
        ]
    }
)