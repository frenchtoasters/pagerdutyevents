from PagerDutyEvents import pagerdutyevents

pdes = pagerdutyevents.PagerDutyEvents(api_key="TESTAPIKEY", integration_key="TESTSERVICEKEY")

events = []

for i in range(10):
    template = pdes.event_template
    template['payload']['custom_details']['alert_id'] = i
    events.append(template)

pdes.load_events(events)

print(pdes.cache)

pdes.create_limited()
